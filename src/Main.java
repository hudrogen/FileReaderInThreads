import java.util.*;
import utils.FileParser;


public class Main {
    public static void main(String[] args) {
        //общий сет для хранения уникальных слов
        //его передаем в каждый поток, по нему синхронизимуемся
        HashSet<String> setOfUniqueWords = new HashSet<>();

        //чтобы остальные потоки закончили своб работу, а в качестве дубликата запомнилось первое слово
        Boolean bol = false;

        LinkedList<String> listOfFileNames = new LinkedList<>();
        listOfFileNames.addAll(Arrays.asList("InputData/Первый.txt", "InputData/Второй.txt", "InputData/Третий.txt"));

        //listOfFileNames = FilesUtility.getAllFileNames();
        for (String fileName: listOfFileNames)
            new FileReaderByScanner(setOfUniqueWords, fileName, bol);
    }
}


class FileReaderByScanner implements Runnable{
    Thread t;
    Boolean bol;
    FileParser parser;
    String fileName;
    ArrayList<String> list; //список слов c одного файла
    Set<String> setUniqueWords;


    public FileReaderByScanner(Set<String> setUniqueWords, String fileName, Boolean b) {
        this.bol = b;
        t = new Thread(this);
        Thread.currentThread().setName("Сканнер для файла " + fileName);
        this.fileName = fileName;
        this.setUniqueWords = setUniqueWords;
        t.start();
    }

    @Override
    public void run() {
        int count = 0;
        //в листе все слова с одного файла
        list = new FileParser(fileName).getWordsFromFile();
        //System.out.println("Список всех слов в файле: " + list );
        for (String s: list){
            if (bol){return;} //после нахождения дубликата установить флаг в тру,
            synchronized (bol) {
                synchronized (setUniqueWords) {
                    if (!setUniqueWords.contains(s)) {
                        setUniqueWords.add(s);
                    } else {
                        bol = true;
                        try {
                            throw new MyException(s);
                        } catch (MyException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
