package utils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by hudrogen on 09.04.17.
 * файлы ищутся в стандартной директории InputData
 * Возвращает список файлов в виде листа
 * Нужен для автоматического подхвата файлов с директории InputData
 */
public class FilesUtility {
    static File userDir = new File("InputData");;
    static ArrayList<String> list = new ArrayList<>();
    public FilesUtility() {
        userDir = new File("InputData");
        list = new ArrayList<>();
    }

    public static ArrayList<String> getAllFileNames(){
        for (String fn: userDir.list()) {
            if (fn != null)
                list.add(fn);

        }
        return list;
    }

    public static void main(String[] args) {
        FilesUtility fu = new FilesUtility();
        System.out.println(fu.getAllFileNames());
    }
}
