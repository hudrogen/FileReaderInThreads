package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Метод, который возвращает в виде листа все слова с файла, предварительно удалив литиницу и знаки препинания
 */
public class FileParser {
    Scanner scanner ;
    ArrayList<String> wordsFromFile;
    String dirtyWord;
    String clearWord;

    public ArrayList<String> getWordsFromFile(){
        return wordsFromFile;
    }

    public FileParser(String filename) {
        wordsFromFile = new ArrayList<String>();
        try {
            scanner = new Scanner(new File(filename));
            while (scanner.hasNext()){
                dirtyWord = scanner.next();
                if (!(RegExs.getClearWord(dirtyWord) == null)) {
                    clearWord = RegExs.getClearWord(dirtyWord);
                    wordsFromFile.add(clearWord.toLowerCase());
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("File " + filename + " not found");
        }

    }

}

