package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hudrogen on 09.04.17.
 * Регулярные выражения
 * есть один недочет, если после кирилической строки идут латинские строки,
 * то предыдущая кирилическая строка дублируется
 */
public class RegExs {
    static String res;
    public static String getClearWord(String s){
        return onlyCyrr(s);
    }

    public static String onlyCyrr(String s) {
        String pattern = "[а-я  А-Я -]+";
        String text = s;
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        while (m.find()) {
            res = text.substring(m.start(), m.end());
        }
        return res;
    }

}
